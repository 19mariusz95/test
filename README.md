# Thesis Management System

TMS - place where your thesis will be safe. Web application created as bachelor project.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install and configure: 

* [PostgreSQL](https://www.postgresql.org)
    * database name: tms
* [JDK 8](https://www.java.com/pl/download)
* [Node.js](https://nodejs.org/en)

### Installing

(optional) Setting backend url

* export environment variable called BASE_URL e.g.
```
export BASE_URL=http:localhost:8080
```

Prepare application.properties by including following configuration

* override default database credentials
```
spring.datasource.url=jdbc:postgresql://localhost:[port]/tms
spring.datasource.username=[username]
spring.datasource.password=[password]
```

Creating executable jar

```
mvn -f backend/pom.xml package
```

## Deployment

As you created jar you can execute it using Java
```
java -jar backend/target/tms.jar
```

## Built With

* [Maven](https://maven.apache.org) - Dependency Management

## Authors

* **Mateusz Kopeć** - [matwoosh](https://github.com/matwoosh)
* **Marek Powroźnik** - [Marekp95](https://github.com/Marekp95)
* **Mariusz Skrabacz** - [19mariusz95](https://github.com/19mariusz95)

